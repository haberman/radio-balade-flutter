import 'package:audioplayers/audioplayers.dart';
import 'package:flare_flutter/flare_cache.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:kiwi/kiwi.dart';

import 'core/i18n.dart';
import 'core/network_info.dart';
import 'core/state_info.dart';
import 'core/usecase.dart';
import 'data/models/broadcaster_model.dart';
import 'generated/locale_base.dart';
import 'injector.dart' as injector;
import 'notifier.dart' as notifier;
import 'presentation/bloc/landing_bloc.dart';
import 'presentation/bloc/preference_bloc.dart';
import 'presentation/bloc/stream_bloc.dart';
import 'presentation/bloc/trip_bloc.dart';
import 'presentation/pages/landing_page.dart';
import 'presentation/pages/stream_page.dart';

// class SimpleBlocDelegate extends BlocDelegate {
//   @override
//   onTransition(Bloc bloc, Transition transition) {
//     super.onTransition(bloc, transition);
//     // print(transition);
//   }
// }

Widget _wrapRoute(KiwiContainer container, Widget child) {
  Widget bloc;

  if (child is LandingPage) {
    bloc = BlocProvider(
      create: (_) => LandingBloc(
        loadContent: container.resolve<FailableUseCase>('loadContent'),
        serializeContent: container.resolve<UseCase>('serializeContent'),
        isGPSUpAndGranted: container.resolve<FailableUseCase>('isGPSUpAndGranted'),
      ),
      child: child,
    );
  } else if (child is StreamPage) {
    bloc = MultiBlocProvider(
      providers: [
        BlocProvider<PreferenceBloc>(
          create: (_) => PreferenceBloc(
            getPreference: container.resolve<UseCase>('getPreference'),
            saveRegion: container.resolve<UseCase>('saveRegion'),
            saveExtent: container.resolve<UseCase>('saveExtent'),
          ),
        ),
        BlocProvider<StreamBloc>(
          create: (_) => StreamBloc(
              getPreference: container.resolve<UseCase>('getPreference'),
              filterBroadcasters: container.resolve<FailableUseCase>('filterBroadcasters'),
              getRandomBroadcaster: container.resolve<FailableUseCase>('getRandomBroadcaster'),
              broadcasterPlaylist: container.resolve<BroadcasterPlaylist>(),
              networkInfo: container.resolve<NetworkInfo>(),
              isGPSUpAndGranted: container.resolve<FailableUseCase>('isGPSUpAndGranted')),
        ),
        BlocProvider<TripBloc>(
          create: (_) => TripBloc(
            watchPosition: container.resolve<FailableUseCase>('watchPosition'),
          ),
        )
      ],
      child: child,
    );
  }

  return Material(
    type: MaterialType.transparency,
    child: Container(
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage('assets/img/bg-noise.png'), repeat: ImageRepeat.repeat),
      ),
      child: bloc,
    ),
  );
}

class App extends StatefulWidget {
  final KiwiContainer container;

  const App({Key key, @required this.container}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    print("disposing");
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.paused:
        widget.container.resolve<StateInfo>().paused = true;
        final broadcaster = widget.container.resolve<BroadcasterPlaylist>().playing;
        if (broadcaster != null) {
          await notifier.show(context, broadcaster.value1);
        }
        break;

      case AppLifecycleState.resumed:
        widget.container.resolve<StateInfo>().paused = false;
        await widget.container.resolve<FlutterLocalNotificationsPlugin>().cancel(0);
        break;

      default:
        widget.container.resolve<FlutterLocalNotificationsPlugin>().cancel(0);
    }

    print('state is: $state');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: {
        '/': (ctx) {
          widget.container.registerSingleton((_) => Localizations.of<LocaleBase>(ctx, LocaleBase));
          return _wrapRoute(widget.container, LandingPage());
        },
        '/stream': (ctx) {
          final broadcasters = ModalRoute.of(ctx).settings.arguments as List<BroadcasterModel>;
          widget.container.resolve<BroadcasterPlaylist>().source = broadcasters;
          return _wrapRoute(widget.container, StreamPage());
        }
      },
      initialRoute: '/',
      localizationsDelegates: [I18n()],
      supportedLocales: [Locale('en', 'US'), Locale('fr', 'FR')],
    );
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setEnabledSystemUIOverlays([]);

  FlareCache.doesPrune = false;
  AudioPlayer.logEnabled = false;

  await injector.setup();
  await notifier.setup();

  runApp(App(container: KiwiContainer()));
}
