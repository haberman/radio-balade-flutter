import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;

class LocaleBase {
  Map<String, dynamic> _data;
  String _path;
  Future<void> load(String path) async {
    _path = path;
    final strJson = await rootBundle.loadString(path);
    _data = jsonDecode(strJson);
    initAll();
  }
  
  Map<String, String> getData(String group) {
    return Map<String, String>.from(_data[group]);
  }

  String getPath() => _path;

  Localelanding _landing;
  Localelanding get landing => _landing;
  Localenotification _notification;
  Localenotification get notification => _notification;
  Localepreference _preference;
  Localepreference get preference => _preference;
  Localestream _stream;
  Localestream get stream => _stream;

  void initAll() {
    _landing = Localelanding(Map<String, String>.from(_data['landing']));
    _notification = Localenotification(Map<String, String>.from(_data['notification']));
    _preference = Localepreference(Map<String, String>.from(_data['preference']));
    _stream = Localestream(Map<String, String>.from(_data['stream']));
  }
}

class Localelanding {
  final Map<String, String> _data;
  Localelanding(this._data);

  String get stateSyncing => _data["stateSyncing"];
  String get stateLoading => _data["stateLoading"];
  String get stateGranting => _data["stateGranting"];
  String get stateLocating => _data["stateLocating"];
  String get stateCompleting => _data["stateCompleting"];
  String get failureNetwork => _data["failureNetwork"];
  String get failureLocation => _data["failureLocation"];
  String get failureGPSDenied => _data["failureGPSDenied"];
  String get failureGPSdisabled => _data["failureGPSdisabled"];
  String get failureData => _data["failureData"];
}
class Localenotification {
  final Map<String, String> _data;
  Localenotification(this._data);

  String get broadcasting => _data["broadcasting"];
  String get tapInfo => _data["tapInfo"];
}
class Localepreference {
  final Map<String, String> _data;
  Localepreference(this._data);

  String get regionTitle => _data["regionTitle"];
  String get regionFr => _data["regionFr"];
  String get regionWorld => _data["regionWorld"];
  String get extentTitle => _data["extentTitle"];
  String get langTitle => _data["langTitle"];
  String get langFr => _data["langFr"];
  String get langEn => _data["langEn"];
}
class Localestream {
  final Map<String, String> _data;
  Localestream(this._data);

  String get awaitNetwork => _data["awaitNetwork"];
  String get awaitLocation => _data["awaitLocation"];
}
