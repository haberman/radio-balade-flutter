import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  final List<dynamic> properties;

  const Failure({this.properties = const <dynamic>[]});

  @override
  get props => [this.properties];
}

class ServerFailure extends Failure {}

class StorageFailure extends Failure {}

class StreamFailure extends Failure {}

enum CriticalFailureType { noConnectivity, noData, noPosition, gpsDenied, gpsDisabled }

class CriticalFailure extends Failure {
  final CriticalFailureType type;
  CriticalFailure(this.type) : super(properties: [type]);
}
