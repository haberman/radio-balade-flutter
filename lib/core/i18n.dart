import 'package:flutter/widgets.dart';

import '../generated/locale_base.dart';

class I18n extends LocalizationsDelegate<LocaleBase> {
  const I18n();
  final idMap = const {'en': 'locales/en_US.json', 'fr': 'locales/fr_FR.json'};

  @override
  bool isSupported(Locale locale) => ['en', 'fr'].contains(locale.languageCode);

  @override
  Future<LocaleBase> load(Locale locale) async {
    var lang = 'en';
    if (isSupported(locale)) lang = locale.languageCode;
    final loc = LocaleBase();
    await loc.load(idMap[lang]);
    return loc;
  }

  @override
  bool shouldReload(I18n old) => false;
}
