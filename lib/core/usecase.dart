import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import 'failure.dart';

abstract class FailableUseCase<T, Params> {
  Future<Either<Failure, T>> call(Params params);
}

abstract class UseCase<T, Params> {
  Future<T> call(Params params);
}

class NoParam extends Equatable {
  @override
  List<Object> get props => null;
}

class SingleParam<T> extends Equatable {
  final T value;

  SingleParam(this.value);

  @override
  List<Object> get props => [value];
}
