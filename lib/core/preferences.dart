part of 'package:radio_balade/data/models/preference_model.dart';

const regionKey = 'region';
const extentKey = 'extent';

const prefValues = {
  extentKey: {
    'short': 25,
    'default': 50,
    'medium': 100,
    'long': 200,
  },
  regionKey: {
    'default': const ['world'],
    'fr': const ['fr', 'be', 'ca']
  }
};
