import 'dart:ui';

import 'package:flutter/material.dart';

abstract class Styles {
  static const double margin = 20;

  static const double fabSize = 64;
  static const double fabIconSize = 38;

  static const double landingIconSize = 72;
  static const double antennaIconSize = 58;

  static const int durationNormal = 250;
  static const int durationSlow = 500;

  static const notifierText = TextStyle(
    fontFamily: 'Open Sans',
    fontSize: 15,
    fontStyle: FontStyle.normal,
  );
  static var notifierWarningText = notifierText.copyWith(color: Colors.white);
  static const headlineText = TextStyle(
    color: Color.fromRGBO(0, 0, 0, 0.8),
    fontFamily: 'Playfair Display',
    fontSize: 21,
  );

  static const radioText = TextStyle(
    fontSize: 15,
    color: Colors.grey,
  );

  static const radioSelectedText = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.bold,
    color: Colors.black,
    decoration: TextDecoration.underline,
  );

  static const audioNameText = TextStyle(
    fontFamily: 'Playfair Display',
    fontWeight: FontWeight.bold,
    fontSize: 26,
    fontStyle: FontStyle.italic,
    color: Colors.white,
  );
  
  static const tripInfoText = TextStyle(
    fontFamily: 'Open Sans',
    fontSize: 11,
    color: Colors.black,
  );

  static const audioBandText = TextStyle(
    fontFamily: 'Open Sans',
    fontSize: 13,
    color: Colors.white,
  );

  static var audioPlaceNameText = audioNameText.copyWith(fontSize: 18, fontStyle: FontStyle.normal);

  static var audioPlaceCountryText = audioBandText.copyWith(fontSize: 15, decoration: TextDecoration.underline);

  static const rainbowPalette = const <Color>[
    Color(0xFF6C88A6),
    Color(0xFF6D9EB3),
    Color(0xFF6EB4C1),
    Color(0xFF6FCBCF),
    Color(0xFF70E1DD),
    Color(0xFF71F8EB),
    Color(0xFF66D5D0),
    Color(0xFF5BB3B5),
    Color(0xFF50919B),
    Color(0xFF4B7E8F),
    Color(0xFF476C84),
    Color(0xFF425A79),
    Color(0xFF3E486E),
    Color(0xFF3A3663),
    Color(0xFF51507A),
    Color(0xFF686A91),
    Color(0xFF7F84A8),
    Color(0xFF969EBF),
    Color(0xFFADB8D6),
    Color(0xFF9AB0CA),
    Color(0xFF87A8BE),
    Color(0xFF75A0B2),
    Color(0xFF6298A6),
    Color(0xFF50919B),
    Color(0xFF62A094),
    Color(0xFF74B08E),
    Color(0xFF86BF87),
    Color(0xFF98CF81),
    Color(0xFFABDF7B),
    Color(0xFF98C872),
    Color(0xFF85B26A),
    Color(0xFF729C62),
    Color(0xFF5F865A),
    Color(0xFF6B8C55),
    Color(0xFF779251),
    Color(0xFF83984D),
    Color(0xFF909E49),
    Color(0xFF9FA244),
    Color(0xFFAFA640),
    Color(0xFFBEAA3C),
    Color(0xFFCEAF38),
    Color(0xFFD6A43B),
    Color(0xFFDE993E),
    Color(0xFFE68F41),
    Color(0xFFEE8444),
    Color(0xFFF67947),
    Color(0xFFFF6F4B),
    Color(0xFFEC5C53),
    Color(0xFFD94A5C)
  ];
}
