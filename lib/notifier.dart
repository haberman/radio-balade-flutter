import 'package:flutter/cupertino.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:kiwi/kiwi.dart';
import 'package:meta/meta.dart';

import 'data/models/broadcaster_model.dart';
import 'generated/locale_base.dart';

class ReceivedNotification {
  final int id;
  final String title;
  final String body;
  final String payload;

  ReceivedNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });
}

String _title(final BroadcasterModel broadcaster) => '<b>${broadcaster.name}</b>, <u>${broadcaster.placeCountry}</u>.';

Future<void> setup() async {
  final kc = KiwiContainer();

  final initializationSettingsAndroid = AndroidInitializationSettings('ic_notification');
  final initializationSettingsIOS = IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
      onDidReceiveLocalNotification: (int id, String title, String body, String payload) async {
        print('onDidReceiveLocalNotification:: $id, $title, $body, $payload');
      });

  final initializationSettings = InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
  await kc.resolve<FlutterLocalNotificationsPlugin>().initialize(initializationSettings, onSelectNotification: (String payload) async {
    if (payload != null) {
      print('onSelectNotification:: $payload');
    }
  });
}

Future<void> show(final BuildContext context, final BroadcasterModel broadcaster) async {
  final kc = KiwiContainer();
  final locale = kc.resolve<LocaleBase>();

  final androidPlatformChannelSpecifics = AndroidNotificationDetails(
    'me.penumbra.radio_balade',
    'Radio Balade',
    'Radio Balade background player',
    playSound: false,
    enableVibration: false,
    channelShowBadge: false,
    styleInformation: DefaultStyleInformation(false, true),
  );

  final iOSPlatformChannelSpecifics = IOSNotificationDetails(presentSound: false);
  final platformChannelSpecifics = NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

  await kc.resolve<FlutterLocalNotificationsPlugin>().show(
        0,
        '${locale.notification.broadcasting} ${_title(broadcaster)}',
        locale.notification.tapInfo,
        platformChannelSpecifics,
      );
}

Future<void> showProgress(final BuildContext context, final BroadcasterModel broadcaster, int progress) async {
  final kc = KiwiContainer();
  final locale = kc.resolve<LocaleBase>();

  final androidPlatformChannelSpecifics = AndroidNotificationDetails(
    'me.penumbra.radio_balade',
    'Radio Balade',
    'Radio Balade background player',
    playSound: false,
    enableVibration: false,
    channelShowBadge: false,
    styleInformation: DefaultStyleInformation(false, true),
    maxProgress: 100,
    progress: progress,
  );

  final iOSPlatformChannelSpecifics = IOSNotificationDetails();
  final platformChannelSpecifics = NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

  await kc.resolve<FlutterLocalNotificationsPlugin>().show(
        0,
        '${locale.notification.broadcasting} ${_title(broadcaster)}',
        locale.notification.tapInfo,
        platformChannelSpecifics,
        payload: '$progress %',
      );
}

Future<void> showIndeterminateProgress(final BuildContext context, final BroadcasterModel broadcaster) async {
  final kc = KiwiContainer();
  final locale = kc.resolve<LocaleBase>();

  final androidPlatformChannelSpecifics = AndroidNotificationDetails(
    'me.penumbra.radio_balade',
    'Radio Balade',
    'Radio Balade background player',
    playSound: false,
    enableVibration: false,
    channelShowBadge: false,
    styleInformation: DefaultStyleInformation(false, true),
    showProgress: true,
    indeterminate: true,
  );

  final iOSPlatformChannelSpecifics = IOSNotificationDetails();
  final platformChannelSpecifics = NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

  await kc.resolve<FlutterLocalNotificationsPlugin>().show(
        0,
        '${locale.notification.broadcasting} ${_title(broadcaster)}',
        locale.notification.tapInfo,
        platformChannelSpecifics,
      );
}
