import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flare_flutter/flare_cache.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../../core/failure.dart';
import '../../core/usecase.dart';
import '../../data/models/broadcaster_model.dart';
import '../../usecases/broadcaster_usecases.dart';
import '../../usecases/geolocator_usecases.dart';

abstract class LandingEvent extends Equatable {
  const LandingEvent();

  @override
  List<Object> get props => [];
}

class LoadContext extends LandingEvent {
  final BuildContext context;

  const LoadContext({@required this.context});

  @override
  List<Object> get props => [context];
}

class LoadData extends LandingEvent {
  final List<String> extensions;
  final bool https;

  const LoadData({
    @required this.extensions,
    @required this.https,
  });

  @override
  List<Object> get props => [extensions, https];
}

class GrantGPS extends LandingEvent {}

class CompleteLanding extends LandingEvent {}

abstract class LandingState extends Equatable {
  const LandingState();

  @override
  List<Object> get props => [];
}

class LandingIdle extends LandingState {}

class LandingSyncing extends LandingState {}

class LandingSynced extends LandingState {
  final List<String> extensions;
  const LandingSynced(this.extensions);

  @override
  List<Object> get props => [extensions];
}

class LandingLoading extends LandingState {}

class LandingLoaded extends LandingState {}

class LandingGranting extends LandingState {}

class LandingGranted extends LandingState {}

class LandingCompleting extends LandingState {}

class LandingCompleted extends LandingState {
  final List<BroadcasterModel> broadcasters;
  const LandingCompleted(this.broadcasters);

  @override
  List<Object> get props => [broadcasters];
}

class LandingFailed extends LandingState {
  final CriticalFailureType cause;
  const LandingFailed(this.cause);

  @override
  List<Object> get props => [cause];
}

class LandingBloc extends Bloc<LandingEvent, LandingState> {
  final LoadContent loadContent;
  final SerializeContent serializeContent;
  final IsGPSUpAndGranted isGPSUpAndGranted;

  // LandingBloc(LandingState initialState) : super(initialState);

  LandingBloc({
    @required this.loadContent,
    @required this.serializeContent,
    @required this.isGPSUpAndGranted,
  }) : super(LandingIdle());

  @override
  Stream<LandingState> mapEventToState(LandingEvent event) async* {
    if (event is LoadContext) {
      final assetBundle = DefaultAssetBundle.of(event.context);

      await cachedActor(AssetFlare(bundle: assetBundle, name: 'assets/flr/syncing.flr'));
      yield LandingSyncing();

      for (final file in const ['downloading', 'locating', 'failing', 'seeking']) {
        await cachedActor(AssetFlare(bundle: assetBundle, name: 'assets/flr/$file.flr'));
      }

      final platform = Theme.of(event.context).platform;
      final extensions = platform == TargetPlatform.iOS ? ['mp3', 'aac', 'ogg'] : ['mp3', 'aac', 'ogg', 'wav'];
      yield LandingSynced(extensions);
    } else if (event is LoadData) {
      yield LandingLoading();

      final jsonContent = await loadContent(LoadParams(extensions: event.extensions, https: event.https));

      yield* jsonContent.fold((failure) async* {
        yield LandingFailed((failure as CriticalFailure).type);
      }, (success) async* {
        yield LandingLoaded();
      });
    } else if (event is GrantGPS) {
      yield LandingGranting();

      final isGranted = await isGPSUpAndGranted(NoParam());

      yield* isGranted.fold((failure) async* {
        yield LandingFailed((failure as CriticalFailure).type);
      }, (success) async* {
        yield LandingGranted();
      });
    } else if (event is CompleteLanding) {
      yield LandingCompleting();

      final jsonContent = await loadContent(null);

      yield* jsonContent.fold((failure) async* {
        yield LandingFailed((failure as CriticalFailure).type);
      }, (success) async* {
        final broadcasters = await serializeContent(SingleParam(success));
        yield LandingCompleted(broadcasters);
      });
    }
  }
}
