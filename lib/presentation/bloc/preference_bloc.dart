import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../../core/usecase.dart';
import '../../data/models/preference_model.dart';
import '../../usecases/preference_usecases.dart';

abstract class PreferenceEvent extends Equatable {
  const PreferenceEvent();

  @override
  List<Object> get props => [];
}

class OpenPreference extends PreferenceEvent {}

class ClosePreference extends PreferenceEvent {}

class UpdatePreference extends PreferenceEvent {
  final String key;
  final String value;

  const UpdatePreference(this.key, this.value);

  @override
  List<Object> get props => [key, value];
}

abstract class PreferenceState extends Equatable {
  const PreferenceState();

  @override
  List<Object> get props => [];
}

class PreferenceClosed extends PreferenceState {}

class PreferenceUpdated extends PreferenceState {
  final bool opened;
  final PreferenceModel preference;

  PreferenceUpdated(this.preference, this.opened);

  @override
  List<Object> get props => [preference, opened];
}

class PreferenceBloc extends Bloc<PreferenceEvent, PreferenceState> {
  final GetPreference getPreference;
  final SaveRegion saveRegion;
  final SaveExtent saveExtent;

  PreferenceBloc({
    @required this.getPreference,
    @required this.saveRegion,
    @required this.saveExtent,
  }) : super(PreferenceClosed());

  @override
  Stream<PreferenceState> mapEventToState(PreferenceEvent event) async* {
    if (event is OpenPreference || event is ClosePreference) {
      final preference = await getPreference(NoParam());
      yield PreferenceUpdated(preference, event is OpenPreference);
    } else if (event is UpdatePreference) {
      if (event.key == regionKey) {
        await saveRegion(SingleParam(event.value));
      } else if (event.key == extentKey) {
        await saveExtent(SingleParam(event.value));
      }
      final preference = await getPreference(NoParam());
      yield PreferenceUpdated(preference, true);
    }
  }
}
