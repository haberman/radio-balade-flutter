import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:kiwi/kiwi.dart';
import 'package:meta/meta.dart';

import '../../data/models/segment_model.dart';
import '../../usecases/geolocator_usecases.dart';

abstract class TripEvent extends Equatable {
  const TripEvent();

  @override
  List<Object> get props => [];
}

class StartTrip extends TripEvent {
  final LocationAccuracy accuracy;
  final int distanceFilter;

  const StartTrip(this.accuracy, this.distanceFilter);

  @override
  List<Object> get props => [accuracy, distanceFilter];
}

class CancelTrip extends TripEvent {}

class UpdateTrip extends TripEvent {
  final Position position;

  const UpdateTrip(this.position);

  @override
  List<Object> get props => [position];
}

abstract class TripState extends Equatable {
  const TripState();

  @override
  List<Object> get props => [];
}

class TripIdle extends TripState {}

class TripUpdating extends TripState {}

class TripUpdated extends TripState {
  final double extent, progress;
  const TripUpdated(this.extent, this.progress);

  @override
  List<Object> get props => [extent, progress];
}

class TripFailed extends TripState {}

class TripBloc extends Bloc<TripEvent, TripState> {
  final WatchPosition watchPosition;

  bool transiting;
  StreamSubscription positionSubscription;

  TripBloc({@required this.watchPosition}) : super(TripIdle()) {
    transiting = false;
  }

  @override
  Stream<TripState> mapEventToState(TripEvent event) async* {
    if (event is StartTrip) {
      final stream = await watchPosition(LocationParams(
        accuracy: event.accuracy,
        distanceFilter: event.distanceFilter,
      ));

      yield* stream.fold((_) async* {
        yield TripFailed();
      }, (success) async* {
        await positionSubscription?.cancel();
        positionSubscription = success.listen((position) {
          if (state is TripIdle || state is TripUpdated) {
            add(UpdateTrip(position));
          }
        });
      });
    } else if (event is CancelTrip) {
      positionSubscription?.cancel();
      yield TripIdle();
    } else if (event is UpdateTrip) {
      yield TripUpdating();
      final segmentHistory = KiwiContainer().resolve<SegmentHistory>();
      final lastSegment = segmentHistory.segments.last;
      final added = lastSegment.add(event.position);

      if (added) {
        yield TripUpdated(lastSegment.extent, lastSegment.extent / segmentHistory.segments.last.limit);
      } else {
        transiting = true;
        add(CancelTrip());
      }
    }
  }

  @override
  Future<void> close() {
    positionSubscription?.cancel();
    return super.close();
  }
}
