import 'dart:async';
import 'dart:math';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

import '../../core/failure.dart';
import '../../core/network_info.dart';
import '../../core/usecase.dart';
import '../../data/models/broadcaster_model.dart';
import '../../data/models/preference_model.dart';
import '../../usecases/broadcaster_usecases.dart';
import '../../usecases/geolocator_usecases.dart';
import '../../usecases/preference_usecases.dart';

abstract class StreamEvent extends Equatable {
  const StreamEvent();

  @override
  List<Object> get props => [];
}

class UpdatePlaylist extends StreamEvent {}

class ChooseStream extends StreamEvent {}

class StartStream extends StreamEvent {
  final int playlistIndex;
  const StartStream(this.playlistIndex);

  @override
  List<Object> get props => [playlistIndex];
}

class UpdateStream extends StreamEvent {
  final double progress;
  const UpdateStream(this.progress);

  @override
  List<Object> get props => [progress];
}

class StopStream extends StreamEvent {
  final int playlistIndex;
  const StopStream(this.playlistIndex);

  @override
  List<Object> get props => [playlistIndex];
}

class MonitorNetwork extends StreamEvent {}

class MonitorGPS extends StreamEvent {}

abstract class StreamState extends Equatable {
  const StreamState();

  @override
  List<Object> get props => [];
}

class StreamIdle extends StreamState {}

class PlaylistReady extends StreamState {
  final List<BroadcasterModel> broadcasters;
  const PlaylistReady(this.broadcasters);

  @override
  List<Object> get props => [broadcasters];
}

class PlaylistConsumed extends StreamState {}

class StreamWorking extends StreamState {
  final Tuple2<BroadcasterModel, int> broadcaster;
  const StreamWorking(this.broadcaster);

  @override
  List<Object> get props => [broadcaster];
}

class StreamChosen extends StreamWorking {
  const StreamChosen(broadcaster) : super(broadcaster);
}

class StreamStarted extends StreamWorking {
  final int prefExtent;
  const StreamStarted(broadcaster, this.prefExtent) : super(broadcaster);

  @override
  List<Object> get props => [broadcaster, prefExtent];
}

class StreamUpdated extends StreamWorking {
  final double progress;
  const StreamUpdated(broadcaster, this.progress) : super(broadcaster);

  @override
  List<Object> get props => [broadcaster, progress];
}

class StreamFailed extends StreamWorking {
  final Failure failure;
  const StreamFailed(this.failure) : super(null);
}

class StreamStopped extends StreamWorking {
  StreamStopped() : super(null);
}

class StreamBloc extends Bloc<StreamEvent, StreamState> {
  final GetPreference getPreference;
  final FilterBroadcasters filterBroadcasters;
  final GetRandomBroadcaster getRandomBroadcaster;
  final BroadcasterPlaylist broadcasterPlaylist;
  final NetworkInfo networkInfo;
  final IsGPSUpAndGranted isGPSUpAndGranted;

  AudioPlayer radioPlayer;
  AudioCache noiseCache;

  StreamSubscription noiseComplete;

  StreamBloc({
    @required this.getPreference,
    @required this.filterBroadcasters,
    @required this.getRandomBroadcaster,
    @required this.broadcasterPlaylist,
    @required this.networkInfo,
    @required this.isGPSUpAndGranted,
  }) : super(StreamIdle()) {
    radioPlayer = AudioPlayer();
    noiseCache = AudioCache(prefix: 'assets/mp3/', fixedPlayer: AudioPlayer());
  }

  @override
  Future<void> close() async {
    if (_isSoundBusy(checkRadio: true)) {
      _releaseRadio(broadcasterPlaylist.playing.value2);
    }
    if (_isSoundBusy(checkNoise: true)) {
      _releaseNoise();
    }

    await super.close();
  }

  bool _isSoundBusy({bool checkRadio = false, bool checkNoise = false}) {
    bool isBusy = false;

    if (checkRadio) {
      isBusy = radioPlayer.state != null && radioPlayer.state != AudioPlayerState.STOPPED;
    }
    if (checkNoise) {
      final noisePlayer = noiseCache.fixedPlayer;
      isBusy = noisePlayer.state != null && noisePlayer.state != AudioPlayerState.STOPPED;
    }

    return isBusy;
  }

  Future<void> _loadNoise() async {
    final rndIdx = 1 + Random().nextInt(9);
    await noiseCache.play('radio_noise-$rndIdx.mp3');

    noiseComplete = noiseCache.fixedPlayer.onPlayerCompletion.listen((_) async {
      await _releaseNoise();
      await _loadNoise();
    });
  }

  Future<void> _releaseNoise() async {
    await noiseComplete?.cancel();
    await noiseCache.fixedPlayer.release();
  }

  Future<void> _releaseRadio(int index) async {
    broadcasterPlaylist.available.removeAt(index);
    broadcasterPlaylist.playing = null;

    await radioPlayer.release();
  }

  @override
  Stream<StreamState> mapEventToState(StreamEvent event) async* {
    if (event is UpdatePlaylist) {
      final preference = await getPreference(NoParam());
      final broadcasters = await filterBroadcasters(FilterParams(
        broadcasters: broadcasterPlaylist.source,
        countryCodes: preference.valueOfRegion,
      ));

      yield* broadcasters.fold((failure) async* {
        yield StreamFailed(failure);
      }, (success) async* {
        broadcasterPlaylist.available = success;
        // prevent radio launch when triggered from a preference change.
        if (!_isSoundBusy(checkRadio: true)) {
          yield PlaylistReady(broadcasterPlaylist.available);
        }
      });
    } else if (event is ChooseStream) {
      final broadcaster = await getRandomBroadcaster(SingleParam(broadcasterPlaylist.available));

      yield* broadcaster.fold((_) async* {
        yield PlaylistConsumed();
      }, (success) async* {
        if (await networkInfo.isConnected) {
          yield StreamChosen(success);
        } else {
          yield StreamFailed(CriticalFailure(CriticalFailureType.noConnectivity));
        }
      });
    } else if (event is StartStream) {
      if (_isSoundBusy(checkRadio: true)) {
        await _releaseRadio(broadcasterPlaylist.playing.value2);
      }

      if (_isSoundBusy(checkNoise: true)) {
        await _releaseNoise();
      }
      await _loadNoise();

      final playlistIndex = event.playlistIndex;
      final broadcaster = broadcasterPlaylist.available[playlistIndex];
      final status = await radioPlayer.play(broadcaster.href);
      radioPlayer.onPlayerError.listen((msg) => print('radioPlayer error: $msg'));

      if (status == 1) {
        try {
          await radioPlayer.onAudioPositionChanged.first.timeout(Duration(seconds: 10));
          await _releaseNoise();

          broadcasterPlaylist.playing = Tuple2(broadcaster, playlistIndex);
          final preference = await getPreference(NoParam());

          yield StreamStarted(broadcasterPlaylist.playing, prefValues[extentKey][preference.extent]);
        } on TimeoutException {
          await _releaseRadio(playlistIndex);
          yield StreamFailed(StreamFailure());
        }
      } else {
        await _releaseRadio(playlistIndex);
        yield StreamFailed(StreamFailure());
      }
    } else if (event is UpdateStream) {
      yield StreamUpdated(broadcasterPlaylist.playing, event.progress);
    } else if (event is StopStream) {
      await _releaseNoise();
      await _releaseRadio(event.playlistIndex);
      yield StreamStopped();
    } else if (event is MonitorNetwork) {
      await Future.doWhile(() => Future.delayed(Duration(milliseconds: 500), () async => !await networkInfo.isConnected));
      if (broadcasterPlaylist.playing != null) {
        add(StartStream(broadcasterPlaylist.playing.value2));
      } else {
        add(ChooseStream());
      }
    }
  }
}
