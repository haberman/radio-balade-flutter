import 'package:flutter/material.dart';

import 'ring_loader.dart';

class BusyIcon extends StatelessWidget {
  final IconData icon;
  final double iconSize;
  final double size;
  final Duration ringSpeed;
  final double borderWidth;
  final Color color;
  final Color backgroundColor;

  const BusyIcon(
      {@required this.icon,
      @required this.iconSize,
      @required this.size,
      @required this.ringSpeed,
      this.borderWidth = 2,
      this.color = Colors.white,
      this.backgroundColor = Colors.transparent});

  Widget build(BuildContext context) {
    return Stack(alignment: AlignmentDirectional.center, children: <Widget>[
      Container(
        width: size,
        height: size,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: backgroundColor,
        ),
      ),
      Icon(icon, size: iconSize, color: Colors.white),
      RingLoader(
        startColor: Colors.white,
        endColor: Colors.white.withAlpha(0),
        size: size,
        speed: ringSpeed,
        strokeWidth: borderWidth,
      )
    ]);
  }
}
