import 'package:flare_dart/math/mat2d.dart';
import 'package:flare_flutter/flare.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/flare_controller.dart';
import 'package:flutter/material.dart';

class FlareLoopController extends StatefulWidget {
  final String file;
  final String animation;
  final VoidCallback onLoop;

  const FlareLoopController(this.file, {this.animation = 'loop', this.onLoop});

  @override
  State<StatefulWidget> createState() => _FlareLoopControllerState();
}

class _FlareLoopControllerState extends State<FlareLoopController> with FlareController {
  ActorAnimation _actorAnimation;
  double _animationTime = 0.0;

  @override
  bool advance(FlutterActorArtboard artboard, double elapsed) {
    _animationTime += elapsed;

    if (_animationTime >= _actorAnimation.duration) {
      _animationTime = 0;
      widget.onLoop();
    }

    _actorAnimation.apply(_animationTime, artboard, 1.0);
    return true;
  }

  @override
  void initialize(FlutterActorArtboard artboard) {
    _actorAnimation = artboard.getAnimation(widget.animation);
  }

  @override
  void setViewTransform(Mat2D viewTransform) {}

  @override
  Widget build(BuildContext context) => FlareActor(widget.file, controller: this);
}
