import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RingLoader extends StatefulWidget {
  final Color startColor;
  final Color endColor;
  final double size;
  final double strokeWidth;
  final Duration speed;

  const RingLoader({
    Key key,
    @required this.startColor,
    @required this.endColor,
    this.size = 50.0,
    this.strokeWidth = 7.0,
    this.speed = const Duration(seconds: 1),
  }) : super(key: key);

  @override
  _RingLoaderState createState() => _RingLoaderState();
}

class _RingLoaderState extends State<RingLoader> with SingleTickerProviderStateMixin {
  AnimationController _animController;
  Animation<double> _animStroke;

  @override
  void initState() {
    super.initState();
    _animController = AnimationController(vsync: this, duration: widget.speed);
    _animStroke = Tween(begin: .0, end: 1.0).animate(
      CurvedAnimation(
        parent: _animController,
        curve: const Interval(.0, 1.0, curve: Curves.linear),
      ),
    )..addListener(() => setState(() {}));

    _animController.repeat();
  }

  @override
  void dispose() {
    _animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox.fromSize(
      size: Size.square(widget.size),
      child: CustomPaint(
        painter: RingPainter(
          strokeWidth: widget.strokeWidth,
          startColor: widget.startColor,
          endColor: widget.endColor,
          progress: _animStroke.value,
        ),
        willChange: true,
      ),
    );
  }
}

class RingPainter extends CustomPainter {
  final double halfPi = .5 * pi;
  final double twoPi = 2 * pi;

  final Paint _paint;

  final double strokeWidth;
  final Color startColor;
  final Color endColor;
  final double progress;

  RingPainter({
    @required this.startColor,
    @required this.endColor,
    this.strokeWidth,
    this.progress,
  }) : _paint = Paint()
          ..style = PaintingStyle.stroke
          ..strokeWidth = strokeWidth
          ..strokeCap = StrokeCap.butt;

  @override
  void paint(Canvas canvas, Size size) {
    final center = Offset(size.width * .5, size.height * .5);
    final radius = (min(size.width, size.height) - strokeWidth) * .5;
    final rect = Rect.fromCircle(center: center, radius: radius);

    final startAngle = progress * twoPi; //-halfPi + startProgress - twoPi;

    final gradient = SweepGradient(
      transform: GradientRotation(startAngle),
      startAngle: 0,
      endAngle: pi,
      colors: [endColor, startColor],
    );

    _paint..shader = gradient.createShader(rect);
    canvas.drawArc(rect, startAngle, pi, false, _paint);
  }

  @override
  bool shouldRepaint(RingPainter oldDelegate) => oldDelegate.progress != progress;
}
