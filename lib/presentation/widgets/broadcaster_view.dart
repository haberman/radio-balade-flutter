import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../../core/failure.dart';
import '../../data/models/broadcaster_model.dart';
import '../../generated/locale_base.dart';
import '../../styles.dart';
import 'busy_icon.dart';

class BroadcasterView extends StatelessWidget {
  final BroadcasterModel broadcaster;
  final int playlistIndex;
  final double streamProgress;
  final CriticalFailure failure;

  const BroadcasterView({this.broadcaster, this.playlistIndex, this.streamProgress, this.failure});

  Widget _playingContent() {
    String band = broadcaster.band;
    final indexOfComma = band.indexOf(',');
    if (indexOfComma != -1) {
      band = band.replaceRange(indexOfComma, band.length, '');
    }

    return IntrinsicWidth(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(Styles.margin),
            decoration: BoxDecoration(
              color: Color.fromARGB(225, 0, 0, 0),
              boxShadow: [BoxShadow(color: Colors.black12, offset: Offset(0, 10), blurRadius: 10)],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  broadcaster.name,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Styles.audioNameText,
                ),
                Text('${broadcaster.frequency} $band', style: Styles.audioBandText),
                SizedBox(height: Styles.margin),
                Text('${broadcaster.placeName}', style: Styles.audioPlaceNameText),
                Text('${broadcaster.placeCountry}', style: Styles.audioPlaceCountryText),
              ],
            ),
          ),
          SizedBox(
            height: 2,
            child: LinearProgressIndicator(
              value: streamProgress,
              backgroundColor: Colors.transparent,
              valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
            ),
          ),
        ],
      ),
    );
  }

  Widget _seekingContent() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          width: Styles.antennaIconSize,
          height: Styles.antennaIconSize,
          child: FlareActor(
            'assets/flr/seeking.flr',
            animation: 'loop',
          ),
        ),
        SizedBox(height: Styles.margin),
        Text(
          broadcaster.name,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: Styles.notifierText.copyWith(color: Colors.white),
        ),
      ],
    );
  }

  Column _failedContent(BuildContext context) {
    final locale = Localizations.of<LocaleBase>(context, LocaleBase);

    IconData iconData;
    String failureMessage;

    if (failure.type == CriticalFailureType.noConnectivity) {
      iconData = Icons.cloud_off;
      failureMessage = locale.stream.awaitNetwork;
    } else {
      iconData = Icons.gps_off;
      failureMessage = locale.stream.awaitLocation;
    }

    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        BusyIcon(
          icon: iconData,
          size: Styles.fabSize,
          iconSize: Styles.fabIconSize * .75,
          ringSpeed: Duration(milliseconds: 750),
        ),
        SizedBox(height: Styles.margin),
        Text(
          failureMessage,
          style: Styles.notifierWarningText,
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget content;

    if (broadcaster != null) {
      content = streamProgress >= 0 ? _playingContent() : _seekingContent();
    } else if (failure != null) {
      content = _failedContent(context);
    }

    return FittedBox(child: content);
  }
}
