import 'package:flutter/material.dart';
import 'package:radio_balade/styles.dart';

enum NotificationLevel { info, warning }

class InfoNotifier extends StatelessWidget {
  final String text;
  final NotificationLevel level;
  final Color borderColor;
  final AssetImage backroundAsset;

  const InfoNotifier({
    @required this.text,
    this.level = NotificationLevel.info,
    this.borderColor = Colors.white,
    this.backroundAsset = const AssetImage('assets/img/bg-notifier-info.png'),
  });

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Styles.notifierText;
    if (level == NotificationLevel.warning) {
      textStyle = textStyle.copyWith(fontWeight: FontWeight.bold);
    }

    return Container(
      padding: const EdgeInsets.all(Styles.margin * .75),
      decoration: BoxDecoration(
          image: DecorationImage(image: backroundAsset, repeat: ImageRepeat.repeat),
          borderRadius: BorderRadius.circular(Styles.margin * 1.5),
          border: Border.all(width: 1, color: borderColor)),
      child: Text(
        text,
        style: textStyle,
        textAlign: TextAlign.center,
        maxLines: 1,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }
}
