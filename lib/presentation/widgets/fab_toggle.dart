import 'package:flutter/material.dart';

class FabToggle extends StatefulWidget {
  final AnimatedIconData icon;
  final Color openColor;
  final Color closeColor;
  final double size;
  final double elevation;
  final Duration duration;
  final ValueChanged<bool> onPressed;

  const FabToggle({
    @required this.icon,
    @required this.openColor,
    @required this.closeColor,
    @required this.onPressed,
    @required this.duration,
    this.elevation = 2,
    this.size = 64,
  });

  @override
  _FabToggleState createState() => _FabToggleState();
}

class _FabToggleState extends State<FabToggle> with SingleTickerProviderStateMixin {
  bool isOpened = false;
  AnimationController _animController;
  Animation<Color> _animColor;
  Animation<double> _animIcon;
  Curve _curve = Curves.easeOut;

  @override
  initState() {
    _animController = AnimationController(vsync: this, duration: widget.duration)..addListener(() => setState(() {}));
    _animIcon = Tween<double>(begin: 0.0, end: 1.0).animate(_animController);
    _animColor = ColorTween(
      begin: widget.closeColor,
      end: widget.openColor,
    ).animate(CurvedAnimation(
      parent: _animController,
      curve: Interval(.0, 1.0, curve: _curve),
    ));
    super.initState();
  }

  @override
  dispose() {
    _animController.dispose();
    super.dispose();
  }

  animate() {
    if (!isOpened) {
      _animController.forward();
    } else {
      _animController.reverse();
    }

    isOpened = !isOpened;
    widget.onPressed(isOpened);
  }

  @override
  Widget build(BuildContext context) => FloatingActionButton(
        elevation: widget.elevation,
        backgroundColor: _animColor.value,
        onPressed: animate,
        child: AnimatedIcon(
          icon: widget.icon,
          progress: _animIcon,
        ),
      );
}
