import 'dart:ui';

import 'package:flutter/material.dart';

class ClippedView extends StatefulWidget {
  const ClippedView({
    @required this.offset,
    @required this.child,
    @required this.decorationImage,
    @required this.duration,
    this.onOpened,
    this.onClosed,
    this.isClipped = false,
  });

  final Offset offset;
  final Widget child;
  final String decorationImage;
  final Duration duration;
  final bool isClipped;
  final VoidCallback onOpened;
  final VoidCallback onClosed;

  @override
  _ClippedViewState createState() => _ClippedViewState();
}

class _ClippedViewState extends State<ClippedView> with SingleTickerProviderStateMixin {
  AnimationController _animController;
  Animation<double> _animPath;

  @override
  void initState() {
    _animController = AnimationController(vsync: this, duration: widget.duration)
      ..addListener(() => setState(() {}))
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed && widget.onOpened != null) {
          widget.onOpened();
        } else if (status == AnimationStatus.dismissed && widget.onClosed != null) {
          widget.onClosed();
        }
      });

    _animPath = Tween<double>(begin: 0.0, end: 1.0).animate(_animController);
    super.initState();
  }

  @override
  void dispose() {
    _animController.dispose();
    super.dispose();
  }

  animate() => !widget.isClipped ? _animController.forward() : _animController.reverse();

  @override
  Widget build(BuildContext context) {
    animate();
    return AnimatedBuilder(
      animation: _animPath,
      builder: (ctx, child) => ClipPath(
        clipper: _RippleClipper(offset: widget.offset, progress: _animPath.value),
        child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(image: AssetImage(widget.decorationImage), fit: BoxFit.cover),
            ),
            child: widget.child),
      ),
    );
  }
}

class _RippleClipper extends CustomClipper<Path> {
  _RippleClipper({@required this.offset, @required this.progress});

  final Offset offset;
  final double progress;

  @override
  Path getClip(Size size) {
    final double diagonal = lerpDouble(10, size.bottomRight(offset).distance, progress);
    final Rect rect = Rect.fromCircle(center: offset, radius: diagonal);

    Path path = Path();
    path.moveTo(offset.dx, offset.dy);
    path.addOval(rect);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(_RippleClipper oldClipper) => progress != oldClipper.progress;
}
