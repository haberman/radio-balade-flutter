import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:radio_balade/styles.dart';

typedef ChangedCallback = Function(RadioModel model);

class RadioModel {
  RadioModel({
    @required this.group,
    @required this.key,
    @required this.value,
    this.isSelected = false,
  });

  final String group;
  final String key;
  final String value;
  
  bool isSelected;
}

class RadioRow extends StatefulWidget {
  RadioRow({this.radios, this.onChanged});

  final ChangedCallback onChanged;
  final List<RadioModel> radios;

  @override
  _RadioRowState createState() => _RadioRowState();
}

class _RadioRowState extends State<RadioRow> {
  List<Widget> _buildRow() {
    List<Widget> row = <Widget>[];

    for (var i = 0; i < widget.radios.length; i++) {
      row.add(Material(
        color: Colors.transparent,
        child: InkWell(
            onTap: () {
              setState(() {
                widget.radios.forEach((radio) => radio.isSelected = false);
                widget.radios[i].isSelected = true;
              });

              widget.onChanged(widget.radios[i]);
            },
            child: RadioItem(widget.radios[i])),
      ));
    }

    return row;
  }

  @override
  Widget build(BuildContext context) => Container(
        margin: EdgeInsets.only(top: Styles.margin * .5, bottom: Styles.margin),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: _buildRow(),
        ),
      );
}

class RadioItem extends StatelessWidget {
  RadioItem(this._radioModel);

  final RadioModel _radioModel;

  @override
  Widget build(BuildContext context) => Container(
        margin: new EdgeInsets.all(Styles.margin * .5),
        child: Text(
          _radioModel.value,
          style: _radioModel.isSelected ? Styles.radioSelectedText : Styles.radioText,
        ),
      );
}
