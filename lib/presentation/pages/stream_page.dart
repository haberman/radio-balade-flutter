import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:kiwi/kiwi.dart';

import '../../notifier.dart' as notifier;
import '../../core/failure.dart';
import '../../core/state_info.dart';
import '../../data/models/broadcaster_model.dart';
import '../../data/models/segment_model.dart';
import '../../styles.dart';
import '../bloc/bloc.dart';
import '../widgets/broadcaster_view.dart';
import '../widgets/fab_toggle.dart';
import 'preference_page.dart';
import 'trip_page.dart';

class StreamPage extends StatelessWidget {
  const StreamPage();

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<StreamBloc>(context);

    return BlocBuilder<StreamBloc, StreamState>(builder: (context, state) {
      BroadcasterModel broadcaster;
      CriticalFailure failure;
      int playlistIndex = -1;
      double streamProgress = -1;

      final appIsPaused = KiwiContainer().resolve<StateInfo>().paused;
      print('app is pause: $appIsPaused');

      if (state is StreamIdle) {
        bloc.add(UpdatePlaylist());
      } else if (state is PlaylistReady) {
        bloc.add(ChooseStream());
      } else if (state is StreamChosen) {
        broadcaster = state.broadcaster.value1;
        playlistIndex = state.broadcaster.value2;
        if (appIsPaused) {
          notifier.showIndeterminateProgress(context, broadcaster);
        }
        bloc.add(StartStream(playlistIndex));
      } else if (state is StreamStarted) {
        broadcaster = state.broadcaster.value1;
        playlistIndex = state.broadcaster.value2;
        streamProgress = 0;

        final radioInfo = '${broadcaster.frequency} ${broadcaster.band}';
        final placeInfo = '${broadcaster.placeName} (${broadcaster.placeCountryCode})';
        final segment = SegmentModel(info: '$radioInfo - $placeInfo', limit: state.prefExtent, positions: []);

        KiwiContainer().resolve<SegmentHistory>().segments.add(segment);
        BlocProvider.of<TripBloc>(context).add(StartTrip(LocationAccuracy.bestForNavigation, 1));
      } else if (state is StreamUpdated) {
        broadcaster = state.broadcaster.value1;
        playlistIndex = state.broadcaster.value2;
        streamProgress = state.progress;
        if (appIsPaused) {
          notifier.showProgress(context, broadcaster, (streamProgress * 100).toInt());
        }
      } else if (state is StreamFailed) {
        if (state.failure is CriticalFailure) {
          failure = state.failure;
          if (failure.type == CriticalFailureType.noConnectivity) {
            bloc.add(MonitorNetwork());
          } else {
            bloc.add(MonitorGPS());
          }
        } else if (state.failure is StreamFailure) {
          bloc.add(ChooseStream());
        }
      }

      final screenSize = MediaQuery.of(context).size;
      return WillPopScope(
        onWillPop: () => Future.value(false),
        child: Stack(children: [
          Container(
            width: screenSize.width,
            height: screenSize.height,
            child: TripPage(),
          ),
          Align(
              alignment: Alignment.center,
              child: BroadcasterView(
                broadcaster: broadcaster,
                playlistIndex: playlistIndex,
                streamProgress: streamProgress,
                failure: failure,
              )),
          PreferencePage(),
          Align(
            alignment: Alignment.topRight,
            child: Padding(
              padding: EdgeInsets.only(top: Styles.margin, right: Styles.margin),
              child: FabToggle(
                  size: Styles.fabSize,
                  icon: AnimatedIcons.menu_close,
                  duration: Duration(milliseconds: Styles.durationNormal),
                  openColor: Colors.grey,
                  closeColor: Colors.black,
                  onPressed: (opened) {
                    final preferenceEvent = opened ? OpenPreference() : ClosePreference();
                    BlocProvider.of<PreferenceBloc>(context).add(preferenceEvent);
                  }),
            ),
          ),
        ]),
      );
    });
  }
}
