import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../core/failure.dart';
import '../../generated/locale_base.dart';
import '../../styles.dart';
import '../bloc/landing_bloc.dart';
import '../widgets/flare_loop_controller.dart';
import '../widgets/info_notifier.dart';

class LandingPage extends StatelessWidget {
  const LandingPage();

  @override
  Widget build(BuildContext context) {
    final locale = Localizations.of<LocaleBase>(context, LocaleBase);
    final bloc = BlocProvider.of<LandingBloc>(context);

    return BlocBuilder<LandingBloc, LandingState>(
      builder: (context, state) {
        NotificationLevel notifLevel = NotificationLevel.info;
        String notifMessage;
        String flareFile;

        if (state is LandingIdle) {
          bloc.add(LoadContext(context: context));
        } else if (state is LandingSyncing || state is LandingSynced) {
          flareFile = 'syncing';
          notifMessage = locale.landing.stateSyncing;
        } else if (state is LandingLoading || state is LandingLoaded) {
          flareFile = 'downloading';
          notifMessage = locale.landing.stateLoading;
        } else if (state is LandingGranting || state is LandingGranted) {
          notifMessage = locale.landing.stateGranting;
          flareFile = 'locating';
        } else if (state is LandingCompleting || state is LandingCompleted) {
          notifMessage = locale.landing.stateCompleting;
          flareFile = 'syncing';
        } else if (state is LandingFailed) {
          flareFile = 'failing';
          notifLevel = NotificationLevel.warning;
          switch (state.cause) {
            case CriticalFailureType.noConnectivity:
              notifMessage = locale.landing.failureNetwork;
              break;
            case CriticalFailureType.noData:
              notifMessage = locale.landing.failureData;
              break;
            case CriticalFailureType.gpsDenied:
            case CriticalFailureType.noPosition:
              notifMessage = locale.landing.failureGPSDenied;
              break;
            case CriticalFailureType.gpsDisabled:
              notifMessage = locale.landing.failureGPSdisabled;
              break;
          }
        }

        List<Widget> children = <Widget>[
          Spacer(flex: 6),
          SizedBox(
            width: Styles.landingIconSize,
            height: Styles.landingIconSize,
            child: flareFile == null
                ? Image.asset('assets/img/ic_app-white.png')
                : notifLevel == NotificationLevel.info
                    ? FlareLoopController(
                        'assets/flr/$flareFile.flr',
                        onLoop: () {
                          if (state is LandingSynced) {
                            bloc.add(LoadData(extensions: state.extensions, https: false));
                          } else if (state is LandingLoaded) {
                            bloc.add(GrantGPS());
                          } else if (state is LandingGranted) {
                            bloc.add(CompleteLanding());
                          }
                        },
                      )
                    : FlareActor('assets/flr/$flareFile.flr', animation: 'anim'),
          ),
        ];

        if (notifMessage == null) {
          children.add(Spacer(flex: 6));
        } else {
          children.addAll([
            Spacer(flex: 1),
            Center(child: InfoNotifier(text: notifMessage, level: notifLevel)),
            Spacer(flex: 5),
          ]);
        }

        return BlocListener<LandingBloc, LandingState>(
          listener: (context, state) {
            if (state is LandingCompleted) {
              Navigator.pushNamed(context, '/stream', arguments: state.broadcasters);
            }
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: children,
          ),
        );
      },
    );
  }
}
