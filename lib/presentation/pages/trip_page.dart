import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kiwi/kiwi.dart';
import 'package:vector_math/vector_math_64.dart';
import 'package:web_mercator/web_mercator.dart' show MercatorViewport, bbox;

import '../../data/models/segment_model.dart';
import '../../styles.dart';
import '../bloc/bloc.dart';

class TripPage extends StatelessWidget {
  const TripPage();

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<TripBloc>(context);

    return BlocBuilder<TripBloc, TripState>(builder: (context, state) {
      final segmentHistory = KiwiContainer().resolve<SegmentHistory>();

      if (state is TripIdle && bloc.transiting) {
        BlocProvider.of<StreamBloc>(context).add(ChooseStream());
        bloc.transiting = false;
      } else if (state is TripUpdated) {
        BlocProvider.of<StreamBloc>(context).add(UpdateStream(state.progress));
      }

      return CustomPaint(painter: TripPainter(segmentHistory.segments, repaint: state is TripUpdated));
    });
  }
}

class ColorPalette {
  final List<Color> colors;
  int colorIndex;
  bool _colorFw = true;

  ColorPalette(this.colors, [this.colorIndex = 0]);

  updateColor() {
    if (_colorFw && colorIndex >= colors.length - 1) {
      _colorFw = false;
    } else if (!_colorFw && colorIndex <= 0) {
      _colorFw = true;
    }

    colorIndex = this._colorFw ? colorIndex + 1 : colorIndex - 1;
  }

  reset() {
    colorIndex = 0;
    _colorFw = true;
  }

  Color get color => colors[colorIndex];
}

class TripPainter extends CustomPainter {
  final List<SegmentModel> segments;
  final bool repaint;
  final ColorPalette colorPalette;

  TripPainter(
    this.segments, {
    this.repaint = false,
    ColorPalette palette,
  }) : colorPalette = palette ?? ColorPalette(Styles.rainbowPalette);

  void _paintSegment(Canvas canvas, MercatorViewport viewport, SegmentModel segment) {
    for (int i = segment.positions.length - 1; i > 0; i--) {
      final from = Vector2(segment.positions[i - 1].longitude, segment.positions[i - 1].latitude);
      final posFrom = viewport.project(from) as Vector2;
      final offsetFrom = Offset(posFrom[0], posFrom[1]);

      final to = Vector2(segment.positions[i].longitude, segment.positions[i].latitude);
      final posTo = viewport.project(to) as Vector2;
      final offsetTo = Offset(posTo[0], posTo[1]);

      canvas.drawLine(
          offsetFrom,
          offsetTo,
          Paint()
            ..color = colorPalette.color
            ..style = PaintingStyle.stroke);

      if (i == 1) {
        canvas.drawCircle(
            offsetFrom,
            5,
            Paint()
              ..color = Color.fromARGB(127, 255, 255, 255)
              ..style = PaintingStyle.fill);

        // final textSpan = TextSpan(text: segment.info, style: Styles.tripInfoText);
        // final textPainter = TextPainter(
        //   text: textSpan,
        //   textDirection: TextDirection.ltr,
        // )..layout();

        // final textWidth = textPainter.width + 16, textHeight = textPainter.height + 12;
        // final textRect = Rect.fromCenter(center: offsetFrom, width: textWidth, height: textHeight);

        // canvas.drawRect(textRect, Paint()..color = Color.fromARGB(255, 255, 255, 255));
        // textPainter.paint(canvas, offsetFrom - Offset(textWidth * .5 - 8, textHeight * .5 - 6));
      } else if (Random().nextDouble() > .75) {
        canvas.drawCircle(
            offsetFrom,
            3,
            Paint()
              ..color = colorPalette.color
              ..style = PaintingStyle.fill);
      }

      colorPalette.updateColor();
    }
  }

  @override
  void paint(Canvas canvas, Size size) {
    if (!repaint || segments == null || segments.length == 0) {
      return;
    }

    final coordinates = segments.map((s) => s.positions).expand((p) => p).toList();
    if (coordinates.length < 2) {
      return;
    }

    final bounds = bbox(coordinates.map((p) => [p.longitude, p.latitude]).toList());
    final viewport = MercatorViewport.fitBounds(
      width: size.width.toInt(),
      height: size.height.toInt(),
      bounds: bounds,
      padding: 20,
    );

    for (int i = segments.length - 1; i >= 0; i--) {
      final segment = segments[i];

      if (i == segments.length - 1) {
        colorPalette.reset();
      }

      if (segment.positions.length >= 2) {
        _paintSegment(canvas, viewport, segment);
      }
    }
  }

  int get length {
    if (segments == null) {
      return 0;
    }
    return segments.map((s) => s.positions).expand((p) => p).length;
  }

  @override
  bool shouldRepaint(TripPainter oldDelegate) => repaint;
}
