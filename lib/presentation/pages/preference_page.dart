import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../data/models/preference_model.dart';
import '../../generated/locale_base.dart';
import '../../styles.dart';
import '../bloc/bloc.dart';
import '../widgets/clipped_view.dart';
import '../widgets/radio_row.dart';

class PreferencePage extends StatelessWidget {
  const PreferencePage();

  String _getRegionValue(LocaleBase locale, String key) {
    switch (key) {
      case 'fr':
        return locale.preference.regionFr;
      case 'default':
        return locale.preference.regionWorld;
      default:
        return '';
    }
  }

  List<RadioModel> _getRadioList(String key, PreferenceModel preference, LocaleBase locale) {
    List<RadioModel> radios = [];
    switch (key) {
      case regionKey:
        radios.addAll(prefValues[regionKey].entries.map((e) => RadioModel(
              group: regionKey,
              key: e.key,
              value: _getRegionValue(locale, e.key),
              isSelected: preference.region == e.key,
            )));
        break;
      case extentKey:
        radios.addAll(prefValues[extentKey].entries.map((e) => RadioModel(
              group: extentKey,
              key: e.key,
              value: '${e.value}m',
              isSelected: preference.extent == e.key,
            )));
        break;
    }
    return radios;
  }

  Widget _buildRadioRow(
    PreferenceBloc bloc,
    String key,
    PreferenceModel preference,
    LocaleBase locale,
  ) =>
      RadioRow(
        radios: _getRadioList(key, preference, locale),
        onChanged: (model) => bloc.add(UpdatePreference(key, model.key)),
      );

  @override
  Widget build(BuildContext context) {
    final Size screen = MediaQuery.of(context).size;
    final Offset clipOffset = Offset(
      screen.width - Styles.margin - Styles.fabSize * .5,
      Styles.margin + Styles.fabSize * .5,
    );

    final locale = Localizations.of<LocaleBase>(context, LocaleBase);
    final bloc = BlocProvider.of<PreferenceBloc>(context);

    return BlocBuilder<PreferenceBloc, PreferenceState>(builder: (context, state) {
      PreferenceModel preference;
      bool isClipped;

      if (state is PreferenceClosed) {
        return Container();
      } else if (state is PreferenceUpdated) {
        isClipped = !state.opened;
        preference = state.preference;
      }

      return ClippedView(
        decorationImage: 'assets/img/bg-noise-invert.png',
        offset: clipOffset,
        duration: Duration(milliseconds: Styles.durationNormal),
        isClipped: isClipped,
        onClosed: () => BlocProvider.of<StreamBloc>(context).add(UpdatePlaylist()),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(locale.preference.regionTitle, style: Styles.headlineText),
              _buildRadioRow(bloc, regionKey, preference, locale),
              Text(locale.preference.extentTitle, style: Styles.headlineText),
              _buildRadioRow(bloc, extentKey, preference, locale),
            ],
          ),
        ),
      );
    });
  }
}
