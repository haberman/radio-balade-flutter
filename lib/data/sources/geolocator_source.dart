import 'package:geolocator/geolocator.dart';
import 'package:meta/meta.dart';
import 'dart:async' show TimeoutException;

import '../../usecases/geolocator_usecases.dart';

@immutable
class GeolocatorSource {
  const GeolocatorSource();

  Future<LocationPermission> get permission async => checkPermission();
  Future<bool> get service async => isLocationServiceEnabled();
  Future<Position> get position async => getCurrentPosition();

  Future<bool> grant() async {
    try {
      await getCurrentPosition(timeLimit: Duration(seconds: 10));
      return true;
    } on PermissionDeniedException {
      return false;
    } on TimeoutException {
      return false;
    }
  }

  Stream<Position> watch(LocationParams params) => getPositionStream(desiredAccuracy: params.accuracy, distanceFilter: params.distanceFilter);
}
