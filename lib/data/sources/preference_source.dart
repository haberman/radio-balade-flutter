import 'package:shared_preferences/shared_preferences.dart';

import '../models/preference_model.dart';

const prefExtentKey = 'PREF_EXTENT';
const prefRegionKey = 'PREF_REGION';

class PreferenceSource {
  final SharedPreferences sharedPreferences;

  const PreferenceSource(this.sharedPreferences);

  PreferenceModel get preference => PreferenceModel(extent: extent, region: region);

  String get extent => sharedPreferences.getString(prefExtentKey) ?? 'default';
  String get region => sharedPreferences.getString(prefRegionKey) ?? 'default';

  Future<bool> saveExtent(String extent) => sharedPreferences.setString(prefExtentKey, extent);
  Future<bool> saveRegion(String region) => sharedPreferences.setString(prefRegionKey, region);
}
