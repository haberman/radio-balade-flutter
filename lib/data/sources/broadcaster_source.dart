import 'dart:io';

import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';

import '../../core/exception.dart';

const storedFileName = 'broadcasters.json';
const apiUrl = 'http://art2.network/works/radio_balade/json/';

class BroadcasterRemoteSource {
  final http.Client httpClient;

  const BroadcasterRemoteSource(this.httpClient);

  Future<String> load({List<String> extensions, bool https}) async {
    final res = await httpClient.post(apiUrl, body: {'extensions': extensions.join(','), 'https': https ? '1' : '0'});

    if (res.statusCode != 200) {
      throw ServerException();
    }

    return res.body;
  }
}

class BroadcasterStorageSource {
  Future<File> get _storedFile async {
    final directory = await getApplicationDocumentsDirectory();
    return File('${directory.path}/$storedFileName.json');
  }

  Future<String> get _storedData async {
    final file = await _storedFile;

    if (await file.exists()) {
      return file.readAsStringSync();
    } else {
      return null;
    }
  }

  Future<String> load() async {
    final content = await _storedData;

    if (content == null) {
      throw StorageException();
    } else {
      return content;
    }
  }

  Future<void> save(String contents) async {
    final file = await _storedFile;
    await file.writeAsString(contents);
  }
}
