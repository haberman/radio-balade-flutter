import 'package:dartz/dartz.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meta/meta.dart';

import '../../core/failure.dart';
import '../../usecases/geolocator_usecases.dart';
import '../sources/geolocator_source.dart';

@immutable
class GeolocatorRepository {
  final GeolocatorSource geolocatorSource;
  const GeolocatorRepository({this.geolocatorSource});

  Future<Either<Failure, bool>> isUpAndGranted() async {
    final permission = await geolocatorSource.permission;

    if (permission == LocationPermission.denied || permission == LocationPermission.deniedForever) {
      final granted = await geolocatorSource.grant();
      if (!granted) {
        return Left(CriticalFailure(CriticalFailureType.gpsDenied));
      }
    } else if (permission == LocationPermission.always || permission == LocationPermission.whileInUse) {
      if (await geolocatorSource.service) {
        return Right(true);
      } else {
        return Left(CriticalFailure(CriticalFailureType.gpsDisabled));
      }
    }

    return Left(CriticalFailure(CriticalFailureType.gpsDisabled));
  }

  Future<Either<Failure, Position>> getPosition() async {
    try {
      final position = await geolocatorSource.position;
      if (position == null) {
        return Left(CriticalFailure(CriticalFailureType.noPosition));
      } else {
        return Right(position);
      }
    } on Exception {
      return Left(CriticalFailure(CriticalFailureType.gpsDenied));
    }
  }

  Either<Failure, Stream<Position>> watchPosition(LocationParams params) {
    try {
      final stream = geolocatorSource.watch(params);
      return Right(stream);
    } on Exception {
      return Left(CriticalFailure(CriticalFailureType.gpsDenied));
    }
  }
}
