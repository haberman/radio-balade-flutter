import 'package:meta/meta.dart';

import '../models/preference_model.dart';
import '../sources/preference_source.dart';

class PreferenceRepository {
  final PreferenceSource source;
  PreferenceRepository({@required this.source});

  Future<PreferenceModel> getPreference() => Future.value(source.preference);

  Future<bool> saveExtent(String extent) async => await source.saveExtent(extent);
  Future<bool> saveRegion(String region) async => await source.saveRegion(region);
}
