import 'dart:convert';
import 'dart:math';

import 'package:dartz/dartz.dart';
import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';

import '../../core/exception.dart';
import '../../core/failure.dart';
import '../../core/network_info.dart';
import '../models/broadcaster_model.dart';
import '../sources/broadcaster_source.dart';

class BroadcasterRepository {
  final BroadcasterRemoteSource remoteSource;
  final BroadcasterStorageSource storageSource;
  final NetworkInfo networkInfo;

  const BroadcasterRepository({
    @required this.remoteSource,
    @required this.storageSource,
    @required this.networkInfo,
  });

  static List<BroadcasterModel> _serializeContent(String content) {
    final jsonMap = json.decode(content).cast<Map<String, dynamic>>();
    return jsonMap.map<BroadcasterModel>((jsonItem) => BroadcasterModel.fromJson(jsonItem)).toList();
  }

  Future<String> _getStoredContent() async {
    try {
      final content = await storageSource.load();
      return content;
    } on StorageException {
      return null;
    }
  }

  Future<String> _getHostedContent(List<String> extensions, bool https) async {
    try {
      final content = await remoteSource.load(extensions: extensions, https: https);
      return content;
    } on ServerException {
      return null;
    }
  }

  Future<Either<Failure, String>> loadContent({
    List<String> extensions,
    bool https,
  }) async {
    String jsonString = await _getStoredContent();

    if (jsonString == null) {
      if (extensions == null && https == null) {
        return Left(CriticalFailure(CriticalFailureType.noData));
      } else if (!(await networkInfo.isConnected)) {
        return Left(CriticalFailure(CriticalFailureType.noConnectivity));
      }

      jsonString = await _getHostedContent(extensions, https);
      if (jsonString == null) {
        return Left(CriticalFailure(CriticalFailureType.noData));
      }

      storageSource.save(jsonString);
    }

    return Right(jsonString);
  }

  Future<List<BroadcasterModel>> serializeContent(String jsonString) async {
    final models = await compute(_serializeContent, jsonString);
    return models;
  }

  Future<Either<Failure, List<BroadcasterModel>>> filterBroadcasters(
    List<BroadcasterModel> broadcasters,
    List<String> countryCodes,
  ) async {
    if (countryCodes.length > 1) {
      broadcasters.removeWhere((model) => !countryCodes.contains(model.placeCountryCode));
    }

    if (broadcasters.length == 0) {
      return Left(CriticalFailure(CriticalFailureType.noData));
    } else {
      return Right(broadcasters);
    }
  }

  Future<Either<Failure, Tuple2<BroadcasterModel, int>>> getRandomBroadcaster(List<BroadcasterModel> broadcasters) async {
    if (broadcasters.length == 0) {
      return Left(CriticalFailure(CriticalFailureType.noData));
    } else {
      final rndIdx = Random().nextInt(broadcasters.length - 1);
      return Right(Tuple2(broadcasters[rndIdx], rndIdx));
    }
  }
}
