import 'package:equatable/equatable.dart';

part 'package:radio_balade/core/preferences.dart';

class PreferenceModel extends Equatable {
  final String extent;
  final String region;

  const PreferenceModel({this.extent, this.region});

  factory PreferenceModel.from(PreferenceModel origin) => PreferenceModel(extent: origin.extent, region: origin.region);

  int get valueOfExtent => prefValues[extentKey][extent];
  List get valueOfRegion => prefValues[regionKey][region];

  @override
  List<Object> get props => [extent, region];

  @override
  String toString() => 'extent: $extent ($valueOfExtent), region: $region ($valueOfRegion)';
}
