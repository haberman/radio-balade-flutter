import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

class BroadcasterModel extends Equatable {
  final String name;
  final String band;
  final double frequency;
  final String href;
  final String placeName;
  final String placeCountry;
  final String placeCountryCode;

  const BroadcasterModel({
    this.name,
    this.band,
    this.frequency,
    this.href,
    this.placeName,
    this.placeCountry,
    this.placeCountryCode,
  });

  @override
  List<Object> get props => [name, band, frequency, href];

  @override
  String toString() {
    return 'name: $name\nband: $band\nfrequency: $frequency\nhref: $href';
  }

  factory BroadcasterModel.fromJson(Map<String, dynamic> json) {
    return BroadcasterModel(
        name: json['radio_name'],
        band: json['radio_band'],
        frequency: json['radio_freq'].toDouble(),
        href: json['stream_href'],
        placeName: json['place_name'],
        placeCountry: json['place_country'],
        placeCountryCode: json['place_country_code']);
  }
}

class BroadcasterPlaylist {
  List<BroadcasterModel> source;
  List<BroadcasterModel> available;

  Tuple2<BroadcasterModel, int> playing;
}

String getBand(final BroadcasterModel broadcaster) {
  String band = broadcaster.band;
  final indexOfComma = band.indexOf(',');
  if (indexOfComma != -1) {
    band = band.replaceRange(indexOfComma, band.length, '');
  }

  return band;
}
