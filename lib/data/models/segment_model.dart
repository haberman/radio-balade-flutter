import 'dart:math';

import 'package:geolocator/geolocator.dart';
import 'package:meta/meta.dart';

/// Calculates the distance between the supplied coordinates in meters.
///
/// The distance between the coordinates is calculated using the Haversine
/// formula (see https://en.wikipedia.org/wiki/Haversine_formula). The
/// supplied coordinates [startLatitude], [startLongitude], [endLatitude] and
/// [endLongitude] should be supplied in degrees.
double distanceBetween(
  double startLatitude,
  double startLongitude,
  double endLatitude,
  double endLongitude,
) {
  var earthRadius = 6378137.0;
  var dLat = _toRadians(endLatitude - startLatitude);
  var dLon = _toRadians(endLongitude - startLongitude);

  var a = pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(_toRadians(startLatitude)) * cos(_toRadians(endLatitude));
  var c = 2 * asin(sqrt(a));

  return earthRadius * c;
}

_toRadians(double degree) {
  return degree * pi / 180;
}

class SegmentModel {
  final String info;
  final int limit;
  final List<Position> positions;

  double _extent;

  SegmentModel({@required this.info, @required this.limit, @required this.positions});

  bool add(Position position) {
    if (positions.length > 0) {
      final lastPosition = positions.last;
      final distance = distanceBetween(
        lastPosition.latitude,
        lastPosition.longitude,
        position.latitude,
        position.longitude,
      );

      if (distance + _extent < limit) {
        positions.add(position);
        _extent += distance;
        return true;
      }
    } else {
      positions.add(position);
      _extent = 0;
      return true;
    }

    return false;
  }

  double get extent => _extent;
}

class SegmentHistory {
  final List<SegmentModel> segments;
  const SegmentHistory(this.segments);

  double extent() {
    double res = 0;
    for (final segment in segments) {
      res += segment.extent;
    }
    return res;
  }
}
