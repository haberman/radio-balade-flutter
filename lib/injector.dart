import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:kiwi/kiwi.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'core/network_info.dart';
import 'core/state_info.dart';
import 'core/usecase.dart';
import 'data/models/broadcaster_model.dart';
import 'data/models/segment_model.dart';
import 'data/repositories/broadcaster_repository.dart';
import 'data/repositories/geolocator_repository.dart';
import 'data/repositories/preference_repository.dart';
import 'data/sources/broadcaster_source.dart';
import 'data/sources/geolocator_source.dart';
import 'data/sources/preference_source.dart';
import 'usecases/broadcaster_usecases.dart';
import 'usecases/geolocator_usecases.dart';
import 'usecases/preference_usecases.dart';

Future<void> setup() async {
  /** UTILITY SINGLETONS */
  final c = KiwiContainer()..silent = true;

  c.registerSingleton((_) => DataConnectionChecker());
  c.registerSingleton((c) => NetworkInfo(c<DataConnectionChecker>()));
  c.registerSingleton((_) => http.Client());
  c.registerSingleton((_) => FlutterLocalNotificationsPlugin());

  final sharedPreferences = await SharedPreferences.getInstance();
  c.registerSingleton((_) => sharedPreferences);

  c.registerSingleton((_) => BroadcasterPlaylist());
  c.registerSingleton((_) => SegmentHistory([]));
  c.registerSingleton((_) => StateInfo(false));

  /** SOURCES */
  c.registerSingleton((_) => BroadcasterStorageSource());
  c.registerSingleton((_) => GeolocatorSource());
  c.registerSingleton((c) => BroadcasterRemoteSource(c<http.Client>()));
  c.registerSingleton((c) => PreferenceSource(c<SharedPreferences>()));

  /** REPOSITORIES */
  c.registerSingleton((c) => BroadcasterRepository(remoteSource: c<BroadcasterRemoteSource>(), storageSource: c<BroadcasterStorageSource>(), networkInfo: c<NetworkInfo>()));
  c.registerSingleton((c) => PreferenceRepository(source: c<PreferenceSource>()));
  c.registerSingleton((c) => GeolocatorRepository(geolocatorSource: c<GeolocatorSource>()));

  /** USECASES */
  c.registerFactory<FailableUseCase>(
    (c) => LoadContent(c<BroadcasterRepository>()),
    name: 'loadContent',
  );
  c.registerFactory<UseCase>(
    (c) => SerializeContent(c<BroadcasterRepository>()),
    name: 'serializeContent',
  );
  c.registerFactory<FailableUseCase>(
    (c) => FilterBroadcasters(c<BroadcasterRepository>()),
    name: 'filterBroadcasters',
  );
  c.registerFactory<FailableUseCase>(
    (c) => GetRandomBroadcaster(c<BroadcasterRepository>()),
    name: 'getRandomBroadcaster',
  );
  c.registerFactory<UseCase>(
    (c) => GetPreference(c<PreferenceRepository>()),
    name: 'getPreference',
  );
  c.registerFactory<UseCase>(
    (c) => SaveRegion(c<PreferenceRepository>()),
    name: 'saveRegion',
  );
  c.registerFactory<UseCase>(
    (c) => SaveExtent(c<PreferenceRepository>()),
    name: 'saveExtent',
  );
  c.registerFactory<FailableUseCase>(
    (c) => IsGPSUpAndGranted(c<GeolocatorRepository>()),
    name: 'isGPSUpAndGranted',
  );
  c.registerFactory<FailableUseCase>(
    (c) => WatchPosition(c<GeolocatorRepository>()),
    name: 'watchPosition',
  );
}
