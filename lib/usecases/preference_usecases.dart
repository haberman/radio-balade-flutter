import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

import '../core/usecase.dart';
import '../data/models/preference_model.dart';
import '../data/repositories/preference_repository.dart';

class PreferenceParams extends Equatable {
  final String region;
  final String extent;
  const PreferenceParams({@required this.region, @required this.extent});

  @override
  List<Object> get props => [region, extent];
}

class GetPreference implements UseCase<PreferenceModel, NoParam> {
  final PreferenceRepository repository;
  GetPreference(this.repository);

  @override
  Future<PreferenceModel> call(NoParam noParam) {
    return repository.getPreference();
  }
}

class SaveRegion implements UseCase<bool, SingleParam> {
  final PreferenceRepository repository;
  SaveRegion(this.repository);

  @override
  Future<bool> call(SingleParam param) {
    return repository.saveRegion(param.value);
  }
}

class SaveExtent implements UseCase<bool, SingleParam> {
  final PreferenceRepository repository;
  SaveExtent(this.repository);

  @override
  Future<bool> call(SingleParam param) {
    return repository.saveExtent(param.value);
  }
}
