import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:geolocator/geolocator.dart';
import 'package:meta/meta.dart';

import '../core/failure.dart';
import '../core/usecase.dart';
import '../data/repositories/geolocator_repository.dart';

class IsGPSUpAndGranted implements FailableUseCase<bool, NoParam> {
  final GeolocatorRepository repository;
  const IsGPSUpAndGranted(this.repository);

  @override
  Future<Either<Failure, bool>> call(NoParam params) => repository.isUpAndGranted();
}

class GetPosition implements FailableUseCase<Position, NoParam> {
  final GeolocatorRepository repository;
  const GetPosition(this.repository);

  @override
  Future<Either<Failure, Position>> call(NoParam params) => repository.getPosition();
}

class LocationParams extends Equatable {
  final LocationAccuracy accuracy;
  final int distanceFilter;

  const LocationParams({@required this.accuracy, @required this.distanceFilter});

  @override
  List<Object> get props => [accuracy];
}

class WatchPosition implements FailableUseCase<Stream<Position>, LocationParams> {
  final GeolocatorRepository repository;
  const WatchPosition(this.repository);

  @override
  Future<Either<Failure, Stream<Position>>> call(LocationParams params) =>
      Future.value(repository.watchPosition(params));
}
