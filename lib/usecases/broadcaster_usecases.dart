import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../core/failure.dart';
import '../core/usecase.dart';
import '../data/models/broadcaster_model.dart';
import '../data/repositories/broadcaster_repository.dart';

class LoadParams extends Equatable {
  final List<String> extensions;
  final bool https;

  const LoadParams({@required this.extensions, @required this.https});

  @override
  List<Object> get props => [extensions, https];
}

class FilterParams extends Equatable {
  final List<BroadcasterModel> broadcasters;
  final List<String> countryCodes;

  FilterParams({@required this.broadcasters, @required this.countryCodes});

  @override
  List<Object> get props => [countryCodes];
}

class LoadContent implements FailableUseCase<String, LoadParams> {
  final BroadcasterRepository repository;
  const LoadContent(this.repository);

  @override
  Future<Either<Failure, String>> call(LoadParams params) async {
    return await repository.loadContent(
      extensions: params?.extensions ?? null,
      https: params?.https ?? null,
    );
  }
}

class SerializeContent implements UseCase<List<BroadcasterModel>, SingleParam<String>> {
  final BroadcasterRepository repository;
  const SerializeContent(this.repository);

  @override
  Future<List<BroadcasterModel>> call(SingleParam param) async {
    return await repository.serializeContent(param.value);
  }
}

class FilterBroadcasters implements FailableUseCase<List<BroadcasterModel>, FilterParams> {
  final BroadcasterRepository repository;
  const FilterBroadcasters(this.repository);

  @override
  Future<Either<Failure, List<BroadcasterModel>>> call(FilterParams params) async {
    return await repository.filterBroadcasters(params.broadcasters, params.countryCodes);
  }
}

class GetRandomBroadcaster implements FailableUseCase<Tuple2<BroadcasterModel, int>, SingleParam> {
  final BroadcasterRepository repository;
  const GetRandomBroadcaster(this.repository);

  @override
  Future<Either<Failure, Tuple2<BroadcasterModel, int>>> call(SingleParam param) async {
    return await repository.getRandomBroadcaster(param.value);
  }
}
