import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:radio_balade/core/usecase.dart';
import 'package:radio_balade/data/models/preference_model.dart';
import 'package:radio_balade/data/repositories/preference_repository.dart';
import 'package:radio_balade/usecases/preference_usecases.dart';

class MockPreferenceRepository extends Mock implements PreferenceRepository {}

void main() {
  MockPreferenceRepository mockPreferenceRepository;

  GetPreference getPreference;
  SaveRegion saveRegion;

  setUp(() {
    mockPreferenceRepository = MockPreferenceRepository();
    getPreference = GetPreference(mockPreferenceRepository);
    saveRegion = SaveRegion(mockPreferenceRepository);
  });

  final tRegion = 'fr';
  final tPreference = PreferenceModel(extent: 'short', region: 'fr');

  group('getPreference', () {
    test('should receive a PreferenceModel from repository', () async {
      when(mockPreferenceRepository.getPreference()).thenAnswer((_) async => tPreference);

      final res = await getPreference(NoParam());
      expect(res, tPreference);

      verify(mockPreferenceRepository.getPreference());
      verifyNoMoreInteractions(mockPreferenceRepository);
    });
  });

  group('savePreference', () {
    test(
      'should save region and receivec an updated model back',
      () async {
        when(mockPreferenceRepository.saveRegion(any)).thenAnswer((_) async => true);

        final res = await saveRegion(SingleParam(tRegion));
        expect(res, true);
        verify(mockPreferenceRepository.saveRegion(any));
        verifyNoMoreInteractions(mockPreferenceRepository);
      },
    );
  });
}
