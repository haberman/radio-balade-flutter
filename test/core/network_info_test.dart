import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:radio_balade/core/network_info.dart';

class MockDataConnectionChecker extends Mock implements DataConnectionChecker {}

void main() {
  MockDataConnectionChecker mockDataConnectionChecker;
  NetworkInfo networkInfo;

  setUp(() {
    mockDataConnectionChecker = MockDataConnectionChecker();
    networkInfo = NetworkInfo(mockDataConnectionChecker);
  });
  group('isConnected', () {
    test('should forward call to DataConnectionChecker.hasConnection', () async {
      final tHasConnection = Future.value(true);
      when(mockDataConnectionChecker.hasConnection).thenAnswer((_) => tHasConnection);

      final res = networkInfo.isConnected;
      verify(mockDataConnectionChecker.hasConnection);
      expect(res, tHasConnection);
    });
  });
}
