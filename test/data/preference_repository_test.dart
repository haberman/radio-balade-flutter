import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:radio_balade/data/models/preference_model.dart';
import 'package:radio_balade/data/repositories/preference_repository.dart';
import 'package:radio_balade/data/sources/preference_source.dart';

class MockSource extends Mock implements PreferenceSource {}

void main() {
  MockSource mockSource;
  PreferenceRepository preferenceRepository;

  setUp(() {
    mockSource = MockSource();
    preferenceRepository = PreferenceRepository(source: mockSource);
  });

  test('should return a PreferenceModel from repository', () async {
    final tPreferenceModel = PreferenceModel(region: 'fr', extent: 'short');
    when(mockSource.preference).thenAnswer((_) => tPreferenceModel);
    
    final res = await preferenceRepository.getPreference();
    verify(mockSource.preference);
    expect(res, equals(tPreferenceModel));
  });
}
