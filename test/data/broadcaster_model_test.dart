import 'dart:convert';
import 'package:flutter_test/flutter_test.dart';
import 'package:radio_balade/data/models/broadcaster_model.dart';

import '../fixtures/fixture_reader.dart';

void main() {
  final tBroadcasterModel = BroadcasterModel(
    name: 'Дарик Носталжи',
    band: 'УКВ,MHz',
    frequency: 90,
    href: 'http://darikradio.by.host.bg:8000/Nostalgie',
    placeName: 'Sofia',
    placeCountry: 'Bulgarie',
    placeCountryCode: 'bg',
  );

  test(
    'should be a subclass of BroadcasterModel',
    () async {
      expect(tBroadcasterModel, isA<BroadcasterModel>());
    },
  );

  test(
    'should return a valid Broadcaster from JSON',
    () async {
      final Map<String, dynamic> jsonMap = json.decode(fixture('broadcaster.json'));

      final res = BroadcasterModel.fromJson(jsonMap);
      expect(res, tBroadcasterModel);
    },
  );
}
