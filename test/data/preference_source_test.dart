import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:radio_balade/data/models/preference_model.dart';
import 'package:radio_balade/data/sources/preference_source.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MockSharedPreferences extends Mock implements SharedPreferences {}

void main() {
  MockSharedPreferences mockSharedPreferences;
  PreferenceSource source;

  setUp(() {
    mockSharedPreferences = MockSharedPreferences();
    source = PreferenceSource(mockSharedPreferences);
  });

  group('getPreference', () {
    final tPreferenceModel = PreferenceModel();

    test('should return region of PreferenceModel from SharedPreferences', () {
      when(mockSharedPreferences.getString(any)).thenReturn(tPreferenceModel.region);

      final res = source.region;
      verify(mockSharedPreferences.getString(prefRegionKey));
      expect(res, tPreferenceModel.region);
    });
    test('should return extent of PreferenceModel from SharedPreferences', () {
      when(mockSharedPreferences.getString(any)).thenReturn(tPreferenceModel.extent);

      final res = source.extent;
      verify(mockSharedPreferences.getString(prefExtentKey));
      expect(res, tPreferenceModel.extent);
    });
  });

  group('savePreference', () {
    final tPreferenceModel = PreferenceModel(extent: 'short', region: 'fr');

    test('should save region to SharedPreferences', () {
      source.saveRegion(tPreferenceModel.region);
      verify(mockSharedPreferences.setString(prefRegionKey, tPreferenceModel.region));
    });
    test('should save extent to SharedPreferences', () {
      source.saveExtent(tPreferenceModel.extent);
      verify(mockSharedPreferences.setString(prefExtentKey, tPreferenceModel.extent));
    });
  });
}
