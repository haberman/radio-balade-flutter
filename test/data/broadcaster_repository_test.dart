import 'package:dartz/dartz.dart';
import 'package:mockito/mockito.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:radio_balade/core/exception.dart';
import 'package:radio_balade/core/failure.dart';
import 'package:radio_balade/core/network_info.dart';
import 'package:radio_balade/data/repositories/broadcaster_repository.dart';
import 'package:radio_balade/data/sources/broadcaster_source.dart';

import '../fixtures/fixture_reader.dart';

class MockBroadcasterRemoteSource extends Mock implements BroadcasterRemoteSource {}

class MockBroadcasterStorageSource extends Mock implements BroadcasterStorageSource {}

class MockNetworkInfo extends Mock implements NetworkInfo {}

void main() {
  BroadcasterRepository repository;
  MockBroadcasterRemoteSource mockRemoteSource;
  MockBroadcasterStorageSource mockStorageSource;
  MockNetworkInfo mockNetworkInfo;

  setUp(() {
    mockRemoteSource = MockBroadcasterRemoteSource();
    mockStorageSource = MockBroadcasterStorageSource();
    mockNetworkInfo = MockNetworkInfo();

    repository = BroadcasterRepository(
      remoteSource: mockRemoteSource,
      storageSource: mockStorageSource,
      networkInfo: mockNetworkInfo,
    );
  });

  void runTestsOnline(Function body) {
    group('device is online', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      body();
    });
  }

  void runTestsOffline(Function body) {
    group('device is offline', () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => false);
      });

      body();
    });
  }

  group('getBroadcasters', () {
    final tExtensions = const ['wav'];
    final tHttps = false;

    final tBroadcastersString = fixture('broadcasters_wav.json');

    test(
      'should check if the device is online',
      () async {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
        await repository.loadContent(extensions: tExtensions, https: tHttps);

        verify(mockNetworkInfo.isConnected);
      },
    );

    runTestsOnline(() {
      test(
        'should return matching data when the call to remote source is successful',
        () async {
          when(mockRemoteSource.load(
            extensions: captureAnyNamed('extensions'),
            https: captureAnyNamed('https'),
          )).thenAnswer((_) async => tBroadcastersString);
          final res = await repository.loadContent(
            extensions: tExtensions,
            https: tHttps,
          );

          verify(mockRemoteSource.load(extensions: tExtensions, https: tHttps));
          expect(tBroadcastersString == res.getOrElse(null), true);
        },
      );

      test(
        'should stored data locally when the call to remote source is successful',
        () async {
          when(mockRemoteSource.load(
            extensions: captureAnyNamed('extensions'),
            https: captureAnyNamed('https'),
          )).thenAnswer((_) async => tBroadcastersString);
          await repository.loadContent(
            extensions: tExtensions,
            https: tHttps,
          );

          verify(mockRemoteSource.load(extensions: tExtensions, https: tHttps));
          verify(mockStorageSource.save(tBroadcastersString));
        },
      );

      test(
        'should return server failure when the call to remote data source is unsuccessful',
        () async {
          when(mockRemoteSource.load(
            extensions: captureAnyNamed('extensions'),
            https: captureAnyNamed('https'),
          )).thenThrow(ServerException());
          final result = await repository.loadContent(
            extensions: tExtensions,
            https: tHttps,
          );

          verify(mockRemoteSource.load(extensions: tExtensions, https: tHttps));
          expect(result, equals(Left(CriticalFailure(CriticalFailureType.noData))));
        },
      );
    });

    runTestsOffline(() {
      test(
        'should return stored data when present',
        () async {
          when(mockStorageSource.load()).thenAnswer((_) async => tBroadcastersString);
          final res = await repository.loadContent(
            extensions: tExtensions,
            https: tHttps,
          );

          verifyZeroInteractions(mockRemoteSource);
          verify(mockStorageSource.load());
          expect(tBroadcastersString == res.getOrElse(null), true);
        },
      );

      test(
        'should return CriticalFailure when there is no stored data',
        () async {
          when(mockStorageSource.load()).thenThrow(StorageException());
          final res = await repository.loadContent(
            extensions: tExtensions,
            https: tHttps,
          );

          verifyZeroInteractions(mockRemoteSource);
          verify(mockStorageSource.load());
          expect(res, equals(Left(CriticalFailure(CriticalFailureType.noConnectivity))));
        },
      );
    });
  });
}
