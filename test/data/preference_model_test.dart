import 'package:flutter_test/flutter_test.dart';
import 'package:matcher/matcher.dart';
import 'package:radio_balade/data/models/preference_model.dart';

void main() {
  final tPreferenceModel = PreferenceModel(extent: 'short', region: 'fr');

  test(
    'should be a subclass of PreferenceModel',
    () async {
      expect(tPreferenceModel, isA<PreferenceModel>());
    },
  );

  group('getValues', () {
    test('should get an integer as the value of the extent', () {
      final extentValue = prefValues[extentKey][tPreferenceModel.extent];

      final res = tPreferenceModel.valueOfExtent;
      expect(res, TypeMatcher<int>());
      expect(res, equals(extentValue));
    });

    test('should get a list of strings as a value of the region', () {
      final regionValue = prefValues[regionKey][tPreferenceModel.region];

      final res = tPreferenceModel.valueOfRegion;
      expect(res, TypeMatcher<List<String>>());
      expect(res, equals(regionValue));
    });
  });
}
