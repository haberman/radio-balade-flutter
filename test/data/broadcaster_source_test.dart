import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/mockito.dart';
import 'package:radio_balade/core/exception.dart';
import 'package:radio_balade/data/sources/broadcaster_source.dart';
import 'package:http/http.dart' as http;

import '../fixtures/fixture_reader.dart';

class MockHttpClient extends Mock implements http.Client {}

void main() {
  MockHttpClient mockHttpClient;
  BroadcasterRemoteSource source;

  void _setUpMockHttpClient(String body, int status) {
    when(mockHttpClient.post(argThat(startsWith(apiUrl)), body: anyNamed('body')))
        .thenAnswer((_) async => http.Response(body, status));
  }

  setUp(() {
    mockHttpClient = MockHttpClient();
    source = BroadcasterRemoteSource(mockHttpClient);
  });

  tearDown(() {
    mockHttpClient.close();
    clearInteractions(mockHttpClient);
  });

  group('getRemoteJson', () {
    final tResponseBody = fixture('broadcasters_wav.json');
    test('should perform a post request to the API endpoint', () async {
      _setUpMockHttpClient(tResponseBody, 200);

      source.load(extensions: ['wav'], https: false);
      verify(mockHttpClient.post(apiUrl, body: {'extensions': 'wav', 'https': '0'}));
    });

    test('should return the string body is the response status is 200', () async {
      _setUpMockHttpClient(tResponseBody, 200);

      final res = await source.load(extensions: ['wav'], https: false);
      expect(res, tResponseBody);
    });

    test('method should throw a ServerException when the request failed (404)', () async {
      _setUpMockHttpClient(tResponseBody, 404);

      final call = source.load;
      expect(() => call(extensions: ['wav'], https: false), throwsA(isInstanceOf<ServerException>()));
    });
  });
}
