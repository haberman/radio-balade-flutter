import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:radio_balade/data/models/broadcaster_model.dart';

List<BroadcasterModel> toBroadcasterModels(String content, List<String> countryCodes) {
  final jsonMap = json.decode(content).cast<Map<String, dynamic>>();
  final broadcasters = jsonMap.map<BroadcasterModel>((jsonItem) => BroadcasterModel.fromJson(jsonItem)).toList();

  broadcasters.removeWhere((broadcaster) => !countryCodes.contains(broadcaster.placeCountryCode));
  return broadcasters;
}

List<Tuple2<BroadcasterModel, bool>> toBroadcasterTuples(List<BroadcasterModel> broadcasters) {
  return broadcasters.map((broadcaster) => Tuple2(broadcaster, false)).toList();
}
